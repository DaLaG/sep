package cz.cvut.fel.lampedan.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public enum Waits
{
    SHORT_WAIT, MEDIUM_WAIT, LONG_WAIT, SUPER_LONG_WAIT;

    private static WebDriverWait superLongWait;
    private static WebDriverWait longWait;
    private static WebDriverWait mediumWait;
    private static WebDriverWait shortWait;


    /**
     * Get value of each waits parameter.
     * @return a specific value how long wait until...
     */
    public WebDriverWait getWebDriverWait()
    {
        switch (this)
        {
            case SHORT_WAIT:
                return shortWait;
            case MEDIUM_WAIT:
                return mediumWait;
            case LONG_WAIT:
                return longWait;
            case SUPER_LONG_WAIT:
                return superLongWait;
            default:
                return null;
        }
    }


    /**
     * Initialization of waits enum.
     * @param driver web driver which is used
     */
    public static final void init(WebDriver driver)
    {
        superLongWait = new WebDriverWait(driver, 30);
        longWait = new WebDriverWait(driver, 10);
        mediumWait = new WebDriverWait(driver, 5);
        shortWait = new WebDriverWait(driver, 1, 100);
    }
}
