package cz.cvut.fel.lampedan.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

public final class SeleniumCommonProcedures
{
    private SeleniumCommonProcedures()
    {
        //Singleton
    }


    /**
     * Initialize a waits ENUM for waiting for component
     * @param driver WebDriver which is used
     */
    public static void init(WebDriver driver)
    {
        Waits.init(driver);
    }


    /**
     * Waiting until condition of element is satisfied
     * @param condition condition to be satisfied
     * @param howLong how long wait until is condition satisfied; possible values: Waits.SHORT_WAIT, Waits.Medium_WAIT, Waits.LONG_WAIT
     */
    public static void waitUntilConditionSatisfied(ExpectedCondition<?> condition, Waits howLong)
    {
        howLong.getWebDriverWait().until(condition);
    }


    /**
     * Waiting until element is shown on page
     * @param by determine by which is element detected
     * @param isExpected if is element expected or not
     * @param howLong how long wait for component; possible values: Waits.SHORT_WAIT, Waits.Medium_WAIT, Waits.LONG_WAIT
     */
    public static void waitUntilElementIsPresent(By by, boolean isExpected, Waits howLong)
    {
        waitUntilConditionSatisfied(
                isExpected ? ExpectedConditions.presenceOfElementLocated(by) : ExpectedConditions.not(ExpectedConditions.presenceOfElementLocated(by)),
                howLong);
    }


    /**
     * Waiting until WebElement as a part of parent element is presented
     * @param locator used to check parent element. For example table with locator By.xpath("//table")
     * @param sub_locator used as parent elementsub_locator used to find child element. For example td By.xpath("./tr/td")
     * @param isExpected if is element expected or not
     * @param howLong wait for component
     */
    public static void waitUntilNestedElementIsPresent(By locator, By sub_locator, boolean isExpected, Waits howLong)
    {
        waitUntilConditionSatisfied(
                isExpected ? ExpectedConditions.presenceOfNestedElementLocatedBy(locator, sub_locator)
                        : ExpectedConditions.not(ExpectedConditions.presenceOfNestedElementLocatedBy(locator, sub_locator)),
                howLong);
    }


    /**
     * Waiting until WebElement as a part of parent element is presented
     * @param locator used as parent element
     * @param sub_locator used as parent elementsub_locator used to find child element. For example td By.xpath("./tr/td")
     * @param isExpected if is element expected or not
     * @param howLong wait for component
     */
    public static void waitUntilNestedElementIsPresent(WebElement locator, By sub_locator, boolean isExpected, Waits howLong)
    {
        waitUntilConditionSatisfied(
                isExpected ? ExpectedConditions.presenceOfNestedElementLocatedBy(locator, sub_locator)
                        : ExpectedConditions.not(ExpectedConditions.presenceOfNestedElementLocatedBy(locator, sub_locator)),
                howLong);
    }


    /**
     * Wait until element does not have attribute with specific value, e.g. same attribute which determine that element is not reading status e.g. that does not contains img != spinner.gif etc.  
     * @param by define element by: id, xpath, linkName etc.
     * @param attribute attribute which determine status of reading element, e.g. src
     * @param value value of attribute, e.g. elementRead;
     * @param isExpected if is of attribute expected
     * @param howLong how long wait until element is read
     */
    public static void waitUntilElementContainsAttValue(By by, String attribute, String value, boolean isExpected, Waits howLong)
    {
        waitUntilConditionSatisfied(
                isExpected ? ExpectedConditions.attributeContains(by, attribute, value) : ExpectedConditions.not(ExpectedConditions.attributeContains(by, attribute, value)),
                howLong);
    }


    /**
     * Wait until element does not have attribute with specific value, e.g. same attribute which determine that element is not reading status e.g. that does not contains img != spinner.gif etc.  
     * @param element the WebElement
     * @param attribute attribute which determine status of reading element, e.g. src
     * @param value value of attribute, e.g. elementRead;
     * @param isExpected if is of attribute expected
     * @param howLong how long wait until element is read
     */
    public static void waitUntilElementContainsAttValue(WebElement element, String attribute, String value, boolean isExpected, Waits howLong)
    {
        waitUntilConditionSatisfied(
                isExpected ? ExpectedConditions.attributeContains(element, attribute, value)
                        : ExpectedConditions.not(ExpectedConditions.attributeContains(element, attribute, value)),
                howLong);
    }


    /**
     * Wait until an element is visible and enabled such that you can click it.
     * @param by used to find the element
     * @param isExpected if is expected that element will be clickable
     * @param howLong wait for component
     */
    public static void waitUntilElementToBeClickable(By by, boolean isExpected, Waits howLong)
    {
        waitUntilConditionSatisfied(
                isExpected ? ExpectedConditions.elementToBeClickable(by) : ExpectedConditions.not(ExpectedConditions.elementToBeClickable(by)),
                howLong);
    }


    /**
     *  Wait until an element is visible and enabled such that you can click it.
     * @param element the webElement
     * @param isExpected if is expected that element will be clickable
     * @param howLong wait for component
     */
    public static void waitUntilElementToBeClickable(WebElement element, boolean isExpected, Waits howLong)
    {
        waitUntilConditionSatisfied(
                isExpected ? ExpectedConditions.elementToBeClickable(element) : ExpectedConditions.not(ExpectedConditions.elementToBeClickable(element)),
                howLong);
    }


    /**
     * Wait until element does not have attribute with specific value, e.g. same attribute which determine that element is not reading status e.g. that does not contains img != spinner.gif etc.  
     * @param by define element by: id, xpath, linkName etc.
     * @param text of element
     * @param isExpected if is of attribute expected
     * @param howLong wait for component
     */
    public static void waitUntilTextToBePresentInElement(By by, String text, boolean isExpected, Waits howLong)
    {
        waitUntilConditionSatisfied(
                isExpected ? ExpectedConditions.textToBePresentInElementLocated(by, text) : ExpectedConditions.not(ExpectedConditions.textToBePresentInElementLocated(by, text)),
                howLong);
    }


    /**
     * Wait until element does not have attribute with specific value, e.g. same attribute which determine that element is not reading status e.g. that does not contains img != spinner.gif etc.
     * @param element the webElement
     * @param text of element
     * @param isExpected if is of attribute expected
     * @param howLong wait for component
     */
    public static void waitUntilTextToBePresentInElement(WebElement element, String text, boolean isExpected, Waits howLong)
    {
        waitUntilConditionSatisfied(
                isExpected ? ExpectedConditions.textToBePresentInElement(element, text) : ExpectedConditions.not(ExpectedConditions.textToBePresentInElement(element, text)),
                howLong);
    }


    /**
     * Wait until alert is presented.
     * @param isPresented if it will be presented or not
     * @param howLong fait for component
     */
    public static void waitUntilAlertIsPresented(boolean isPresented, Waits howLong)
    {
        waitUntilConditionSatisfied(
                isPresented ? ExpectedConditions.alertIsPresent() : ExpectedConditions.not(ExpectedConditions.alertIsPresent()),
                howLong);
    }


    /**
     * Wait until an element is visible and enabled such that you can click it.
     * @param by used to find the element
     * @param isExpected if is expected that element will be clickable
     * @param howLong wait for component
     */
    public static void waitUntilElementIsDisplayed(By by, boolean isExpected, Waits howLong)
    {
        waitUntilConditionSatisfied(
                isExpected ? ExpectedConditions.visibilityOfElementLocated(by) : ExpectedConditions.not(ExpectedConditions.visibilityOfElementLocated(by)),
                howLong);
    }


    /**
     * Switch to content frame in  HTML document
     * @param driver WebDriver which is used
     * @param howLong wait for component
     */
    public static void switchToIframe(WebDriver driver, Waits howLong)
    {
        driver.switchTo().defaultContent();
        By by = By.id("iframeId");
        waitUntilElementIsPresent(by, true, howLong);
        driver.switchTo().frame(driver.findElement(by));
    }


    /**
     * Click to element which is determined by "by", before click the procedure waits for component
     * @param driver WebDriver which is used
     * @param by define by what should be identify element, e.g. id xpath, css selector etc.
     * @param howLong how long wait before click; possible values: Waits.SHORT_WAIT, Waits.Medium_WAIT, Waits.LONG_WAIT
     * @param hardEvent TODO
     */
    public static void clickToElement(WebDriver driver, By by, Waits howLong)
    {
        waitUntilElementIsPresent(by, true, howLong);
        clickToElement(driver, driver.findElement(by), false);
    }


    /**
     * Click to element, if possible use clickToElement(WebDriver driver, By by, Waits howLong) instead
     * @param driver the WebDriver 
     * @param element the webElement
     * @param hardEvent TODO
     */
    public static void clickToElement(WebDriver driver, WebElement element, boolean hardEvent)
    {
        try
        {
            if (hardEvent)
                throw new Exception();
            else
                element.click();
        }
        catch (Exception e)
        {
            Actions action = new Actions(driver);
            action.moveToElement(element).click().perform();
        }
    }


    /**
     * Determine if is element displayed on screen or it is not.
     * @param driver WebDriver which is used.
     * @param by which element should be displayed
     * @param isExpected if is expected that element is displayed
     * @return if is element in expected displayed state
     */
    public static boolean isElementDisplayed(WebDriver driver, By by, boolean isExpected, Waits howLong)
    {
        waitUntilElementIsPresent(by, true, howLong);
        return !(driver.findElement(by).isDisplayed() ^ isExpected);
    }


    /**
     * Determine if is element enabled on screen or it is not. 
     * @param driver WebDriver which is used.
     * @param by which element should be enabled
     * @param isExpected if is expected that element is enabled
     * @param howLong how long wait until element will be presented
     * @return if is element in expected displayed state
     */
    public static boolean isElementEnabled(WebDriver driver, By by, boolean isExpected, Waits howLong)
    {
        waitUntilElementIsPresent(by, true, howLong);
        return !(driver.findElement(by).isEnabled() ^ isExpected);
    }


    /**
     * Check if is element determined by "by" selected.
     * @param drive webDriver which is used
     * @param by how identify element, e.g. by id, xpath etc.
     * @param howLong how long wait to element
     * @return true - if element is selected, false - if it is not
     */
    public static boolean isElementSelected(WebDriver driver, By by, Waits howLong)
    {
        waitUntilElementIsPresent(by, true, howLong);
        return driver.findElement(by).isSelected();
    }


    /**
     * Get the visible (i.e. not hidden by CSS) innerText of this element, including sub-elements, without any leading or trailing whitespace.
     * @param driver webDriver which is used
     * @param by how identify element, e.g. by id, xpath etc.
     * @param howLong how long wait to element
     * @return text of element
     */
    public static String getElementText(WebDriver driver, By by, Waits howLong)
    {
        waitUntilElementIsPresent(by, true, howLong);
        return driver.findElement(by).getText();
    }


    /**
     * Get the value of the given attribute of the element. Will return the current value, even if this has been modified after the page has been loaded. 
     * @param driver webDriver which is used
     * @param by how identify element, e.g. by id, xpath etc.
     * @param attribute the attribute
     * @param howLong wait for component
     * @return value of the given attribute
     */
    public static String getAttribute(WebDriver driver, By by, String attribute, Waits howLong)
    {
        waitUntilElementIsPresent(by, true, howLong);
        return driver.findElement(by).getAttribute(attribute);
    }


    /**
     * Switch to pop-up frame in  HTML document
     * @param driver WebDriver which is used
     * @param howLong wait for component
     */
    public static void switchToPopUpFrame(WebDriver driver, Waits howLong)
    {
        driver.switchTo().defaultContent();
        By by = By.id("infoPopupContent");
        waitUntilElementIsPresent(by, true, howLong);
        driver.switchTo().frame(driver.findElement(by));
    }


    /**
     *  Get the value of this element as string
     * @param driver webDriver which is used
     * @param by how identify element, e.g. by id, xpath etc.
     * @param howLong how long wait to element
     * @return value of element
     */
    public static String getInputValue(WebDriver driver, By by, Waits howLong)
    {
        return getAttribute(driver, by, "value", howLong);
    }


    /**
     * Insert text
     * @param driver    WebDriver which is used
     * @param by        define how identify element
     * @param inputText input text
     * @param howLong   wait for component
     */
    public static void sendKeys(WebDriver driver, By by, String inputText, Waits howLong)
    {
        waitUntilElementIsPresent(by, true, howLong);
        driver.findElement(by).sendKeys(inputText);
    }


    public static void cleanInputElement(WebDriver driver, By by, Waits howLong)
    {
        waitUntilElementIsPresent(by, true, howLong);
        driver.findElement(by).clear();
    }
}
