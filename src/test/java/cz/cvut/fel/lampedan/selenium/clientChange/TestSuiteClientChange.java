package cz.cvut.fel.lampedan.selenium.clientChange;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.cvut.fel.lampedan.ManageRequestColletorApplication;
import cz.cvut.fel.lampedan.selenium.SeleniumCommonProcedures;
import cz.cvut.fel.lampedan.selenium.SeleniumConstants;
import cz.cvut.fel.lampedan.selenium.Waits;

@RunWith(Suite.class)
@SuiteClasses({
        TestCaseChangeOneEntry.class,
        TestCaseChangeMoreEntry.class,
        TestCaseChangeAllEntry.class

})
public class TestSuiteClientChange
{
    private static final Logger log = LoggerFactory.getLogger(ManageRequestColletorApplication.class);

    private static WebDriver driver;


    @BeforeClass
    public static void setUp()
    {
        System.setProperty("webddriver.chrome.driver", SeleniumConstants.CHROME_DRIVER_PATH);

        ChromeOptions options = new ChromeOptions();
        options.setBinary(SeleniumConstants.CHROME_PATH);
        options.addArguments("start-maximized");

        driver = new ChromeDriver(options);
        SeleniumCommonProcedures.init(driver);

        driver.get(SeleniumConstants.BASE_URL + "\\login");

        SeleniumCommonProcedures.sendKeys(driver, By.id("username"), "user", Waits.MEDIUM_WAIT);
        SeleniumCommonProcedures.sendKeys(driver, By.id("password"), "password", Waits.MEDIUM_WAIT);
    }


    @AfterClass
    public static void tearDown()
    {
        driver.close();
        driver.quit();
        log.info("Testing finished");
    }


    /**
     * @return the driver
     */
    public static WebDriver getDriver()
    {
        return driver;
    }


    /**
     * Sets the driver value.
     * @param driver the driver to set
     */
    public static void setDriver(WebDriver driver)
    {
        TestSuiteClientChange.driver = driver;
    }

}
