package cz.cvut.fel.lampedan.selenium;

public class SeleniumConstants
{
    public static final String BASE_URL = "http://localhost:8080";
    public static final String CHROME_PATH = "c:\\Program Files (x86)\\google\\Chrome\\Application\\chrome.exe";
    public static final String CHROME_DRIVER_PATH = "c:\\Users\\Daniel\\Documents\\Workspace\\chromedrive_v2.19\\chromedrive.exe";

}
