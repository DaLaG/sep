/**
 * Copyright 2016 Bosch Automotive Service Solutions Ltd.
 * All Rights reserved.
 */
package cz.cvut.fel.lampedan.selenium.clientChange;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import cz.cvut.fel.lampedan.selenium.SeleniumCommonProcedures;
import cz.cvut.fel.lampedan.selenium.Waits;

/**
 * @author Daniel
 *
 */
public class TestCaseChangeAllEntry
{
    private WebDriver driver;


    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
        driver = TestSuiteClientChange.getDriver();
    }


    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
        driver.switchTo().defaultContent();
        TestSuiteClientChange.setDriver(driver);
    }


    @Test
    public void test()
    {
        SeleniumCommonProcedures.switchToIframe(driver, Waits.MEDIUM_WAIT);
        SeleniumCommonProcedures.clickToElement(driver, By.id("clientDetail_5"), Waits.SHORT_WAIT);
        SeleniumCommonProcedures.cleanInputElement(driver, By.id("postalCode"), Waits.MEDIUM_WAIT);
        SeleniumCommonProcedures.sendKeys(driver, By.id("postalCode"), "97405", Waits.SHORT_WAIT);
        SeleniumCommonProcedures.cleanInputElement(driver, By.id("country"), Waits.MEDIUM_WAIT);
        SeleniumCommonProcedures.sendKeys(driver, By.id("country"), "Slovensko", Waits.SHORT_WAIT);
        SeleniumCommonProcedures.cleanInputElement(driver, By.id("streetNum"), Waits.MEDIUM_WAIT);
        SeleniumCommonProcedures.sendKeys(driver, By.id("streetNum"), "15", Waits.SHORT_WAIT);
        SeleniumCommonProcedures.clickToElement(driver, By.id("submitForm"), Waits.SHORT_WAIT);

    }

}
