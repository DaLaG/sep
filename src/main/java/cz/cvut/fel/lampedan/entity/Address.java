package cz.cvut.fel.lampedan.entity;

import java.io.Serializable;

import javax.jdo.annotations.ForeignKey;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * 
 * @author Daniel Lamper
 *
 */
@Entity
public class Address implements Serializable
{

    private static final long serialVersionUID = 1L;

    @Id
    @ForeignKey
    private Long userId;
    private String streetName;
    private int streetNum;
    private String postalCode;
    private String cityPart;
    private String city;
    private String county;
    private String country;


    protected Address()
    {

    }


    public Address(Long id, String streetName, int streetNum, String postalCode, String city, String country)
    {
        super();
        this.userId = id;
        this.streetName = streetName;
        this.streetNum = streetNum;
        this.postalCode = postalCode;
        this.city = city;
        this.country = country;
    }


    public Address(Long id, String streetName, int streetNum, String postalCode, String cityPart, String city, String county, String country)
    {
        super();
        this.userId = id;
        this.streetName = streetName;
        this.streetNum = streetNum;
        this.postalCode = postalCode;
        this.cityPart = cityPart;
        this.city = city;
        this.county = county;
        this.country = country;
    }


    public Long getUser_id()
    {
        return userId;
    }


    public void setUser_id(Long user_id)
    {
        this.userId = user_id;
    }


    public String getStreetName()
    {
        return streetName;
    }


    public void setStreetName(String streetName)
    {
        this.streetName = streetName;
    }


    public int getStreetNum()
    {
        return streetNum;
    }


    public void setStreetNum(int streetNum)
    {
        this.streetNum = streetNum;
    }


    public String getPostalCode()
    {
        return postalCode;
    }


    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }


    public String getCityPart()
    {
        return cityPart;
    }


    public void setCityPart(String cityPart)
    {
        this.cityPart = cityPart;
    }


    public String getCity()
    {
        return city;
    }


    public void setCity(String city)
    {
        this.city = city;
    }


    public String getCounty()
    {
        return county;
    }


    public void setCounty(String county)
    {
        this.county = county;
    }


    public String getCountry()
    {
        return country;
    }


    public void setCountry(String country)
    {
        this.country = country;
    }


    @Override
    public String toString()
    {
        return "Address (" + (userId != null ? userId + ", " : "") + (streetName != null ? streetName + ", " : "")
                + streetNum + ", " + (postalCode != null ? postalCode + ", " : "")
                + (cityPart != null ? cityPart + ", " : "") + (city != null ? city + ", " : "")
                + (county != null ? county + ", " : "") + (country != null ? country : "") + ")";
    }

}
