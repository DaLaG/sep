package cz.cvut.fel.lampedan.entity;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.StringWriter;
import java.sql.Timestamp;

import javax.jdo.annotations.ForeignKey;
import javax.jdo.annotations.PrimaryKey;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import cz.cvut.fel.lampedan.ManageRequestColletorApplication;

/**
 * 
 * @author Daniel Lamper
 *
 */
@Entity
public class Request implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @PrimaryKey
    private Long id;

    @ForeignKey
    private Long userId;
    private String createdByUser;
    private Timestamp signingDate;
    private String requestType;
    @Column(columnDefinition = "text")
    private String xml_customerDetailType;
    @Column(columnDefinition = "text")
    private String xml_customerType;
    @Column(columnDefinition = "text")
    private String xml_addressType;
    @Column(columnDefinition = "text")
    private String xml_phoneType;


    protected Request()
    {
        //Singleton
    }


    public Request(Long userId, String createdByUser, String requestType, Customer c, Address a, Phone p)
    {
        this.userId = userId;
        this.createdByUser = createdByUser;
        this.requestType = requestType;
        getCustomerType(c);
        this.xml_customerDetailType = getCustomerDetailType(c, a, p);
        this.signingDate = new Timestamp(System.currentTimeMillis());
    }


    public Element getPhoneType(Document doc, Phone p)
    {
        try
        {

            Element phone = doc.createElement("phoneNum");
            /*doc.appendChild(phone);*/

            Element phoneNumberType = doc.createElement("phoneNumberType");
            phoneNumberType.appendChild(doc.createTextNode(p.getPhoneNumberType() + ""));
            phone.appendChild(phoneNumberType);

            Element phoneNum = doc.createElement("phoneNum");
            phoneNum.appendChild(doc.createTextNode(p.getPhoneNum() + ""));
            phone.appendChild(phoneNum);

            if (p.getCityCode() != null)
            {
                Element cityCode = doc.createElement("cityCode");
                cityCode.appendChild(doc.createTextNode(p.getCityCode() + ""));
                phone.appendChild(cityCode);
            }
            if (p.getCountryCode() != null)
            {
                Element countryCode = doc.createElement("countryCode");
                countryCode.appendChild(doc.createTextNode(p.getCountryCode()));
                phone.appendChild(countryCode);
            }

            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            //transformer.setOutputProperty(OutputKeys.O, "ISO-8859-1");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(phone), new StreamResult(writer));
            xml_phoneType = writer.getBuffer().toString();

            return phone;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }


    public Element getAddressType(Document doc, Address a)
    {
        try
        {
            Element address = doc.createElement("address");
            /*doc.appendChild(address);*/

            Element streetName = doc.createElement("streetName");
            streetName.appendChild(doc.createTextNode(a.getStreetName()));
            address.appendChild(streetName);

            Element streetNum = doc.createElement("streetNum");
            streetNum.appendChild(doc.createTextNode("" + a.getStreetNum()));
            address.appendChild(streetNum);

            Element postalCode = doc.createElement("postalCode");
            postalCode.appendChild(doc.createTextNode(a.getPostalCode()));
            address.appendChild(postalCode);

            if (a.getCityPart() != null)
            {
                Element cityPart = doc.createElement("cityPart");
                cityPart.appendChild(doc.createTextNode(a.getCityPart()));
                address.appendChild(cityPart);
            }

            Element city = doc.createElement("city");
            city.appendChild(doc.createTextNode(a.getCity()));
            address.appendChild(city);

            if (a.getCounty() != null)
            {
                Element county = doc.createElement("county");
                county.appendChild(doc.createTextNode(a.getCounty()));
                address.appendChild(county);
            }

            Element country = doc.createElement("country");
            country.appendChild(doc.createTextNode(a.getCountry()));
            address.appendChild(country);

            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            //transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(address), new StreamResult(writer));
            xml_addressType = writer.getBuffer().toString();
            return address;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }


    public Element getCustomerType(Customer c)
    {
        Element customer = null;
        try
        {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();

            customer = doc.createElement("sequence");
            doc.appendChild(customer);

            Element firstName = doc.createElement("firstName");
            firstName.appendChild(doc.createTextNode(c.getFirstName()));
            customer.appendChild(firstName);

            Element surname = doc.createElement("surname");
            surname.appendChild(doc.createTextNode(c.getSurname()));
            customer.appendChild(surname);

            Element id = doc.createElement("ID");
            id.appendChild(doc.createTextNode(c.getId().toString()));
            customer.appendChild(id);

            Element status = doc.createElement("status");
            status.appendChild(doc.createTextNode(c.getStatus()));
            customer.appendChild(status);

            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            //transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
            xml_customerType = writer.getBuffer().toString();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return customer;
    }


    public String getCustomerDetailType(Customer c, Address a, Phone p)
    {
        String xml = null;
        try
        {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();

            Element rootElement = doc.createElement("fel:CreateCustomerChangeOrder");
            doc.appendChild(rootElement);

            Element requestType = doc.createElement("requestType");
            requestType.appendChild(doc.createTextNode(this.requestType));
            rootElement.appendChild(requestType);

            // 
            Element id = doc.createElement("id");
            id.appendChild(doc.createTextNode(this.userId + ""));
            rootElement.appendChild(id);

            Element customer = doc.createElement("customer");
            rootElement.appendChild(customer);

            Element firstName = doc.createElement("firstName");
            firstName.appendChild(doc.createTextNode(c.getFirstName()));
            customer.appendChild(firstName);

            Element surname = doc.createElement("surname");
            surname.appendChild(doc.createTextNode(c.getSurname()));
            customer.appendChild(surname);

            customer.appendChild(getAddressType(doc, a));

            customer.appendChild(getPhoneType(doc, p));

            Element birthNum = doc.createElement("birthNum");
            birthNum.appendChild(doc.createTextNode(c.getNIN()));
            customer.appendChild(birthNum);

            Element countryOfOrigin = doc.createElement("countryOfOrigin");
            countryOfOrigin.appendChild(doc.createTextNode(c.getBirthPlace()));
            customer.appendChild(countryOfOrigin);

            // write the content into xml file
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            //transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
            xml = writer.getBuffer().toString();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return xml;
    }


    public SOAPMessage getCustomerDetailTypeDocument()
    {
        String toMessage = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:fel=\"http://www.cvut.cz/FEL/\">"
                + "<soapenv:Header/>"
                + "<soapenv:Body>"
                + xml_customerDetailType
                + "</soapenv:Body>"
                + "</soapenv:Envelope>";
        try
        {

            InputStream is = new ByteArrayInputStream(toMessage.getBytes());
            SOAPMessage request = MessageFactory.newInstance().createMessage(null, is);
            String serverURI = "http://www.cvut.cz/FEL/";
            MimeHeaders headers = request.getMimeHeaders();
            headers.addHeader("SOAPAction", serverURI);

            request.saveChanges();

            /* Print the request message */
            Logger log = LoggerFactory.getLogger(ManageRequestColletorApplication.class);
            log.info("Request SOAP Message = ");
            request.writeTo(System.out);
            return request;
        }
        catch (SOAPException | IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }


    public Long getId()
    {
        return id;
    }


    public void setId(Long id)
    {
        this.id = id;
    }


    public Long getUserId()
    {
        return userId;
    }


    public void setUserId(Long userId)
    {
        this.userId = userId;
    }


    /**
     * @return the createdByUser
     */
    public String getCreatedByUser()
    {
        return createdByUser;
    }


    /**
     * Sets the createdByUser value.
     * @param createdByUser the createdByUser to set
     */
    public void setCreatedByUser(String createdByUser)
    {
        this.createdByUser = createdByUser;
    }


    /**
     * @return the signingDate
     */
    public Timestamp getSigningDate()
    {
        return signingDate;
    }


    /**
     * Sets the signingDate value.
     * @param signingDate the signingDate to set
     */
    public void setSigningDate(Timestamp signingDate)
    {
        this.signingDate = signingDate;
    }


    public String getXml_customerDetailType()
    {
        return xml_customerDetailType;
    }


    public void setXml_customerDetailType(String xml_customerDetailType)
    {
        this.xml_customerDetailType = xml_customerDetailType;
    }


    /**
     * @return the requestType
     */
    public String getRequestType()
    {
        return requestType;
    }


    /**
     * Sets the requestType value.
     * @param requestType the requestType to set
     */
    public void setRequestType(String requestType)
    {
        this.requestType = requestType;
    }


    /**
     * @return the xml_phoneType
     */
    public String getXml_phoneType()
    {
        return xml_phoneType;
    }


    public void setXml_phoneType(String xml_phoneType)
    {
        this.xml_phoneType = xml_phoneType;
    }


    public String getXml_customerType()
    {
        return xml_customerType;
    }


    public void setXml_customerType(String xml_customerType)
    {
        this.xml_customerType = xml_customerType;
    }


    public String getXml_addressType()
    {
        return xml_addressType;
    }


    public void setXml_addressType(String xml_addressType)
    {
        this.xml_addressType = xml_addressType;
    }


    public String getXml_PhoneType()
    {
        return xml_phoneType;
    }


    public void setXml_PhoneType(String xml_PhoneType)
    {
        this.xml_phoneType = xml_PhoneType;
    }


    @Override
    public String toString()
    {
        return "Request (" + (id != null ? id + ", " : "") + (userId != null ? userId + ", " : "") + (xml_customerDetailType != null ? xml_customerDetailType + ", " : "")
                + (xml_customerType != null ? xml_customerType + ", " : "") + (xml_addressType != null ? xml_addressType + ", " : "") + (xml_phoneType != null ? xml_phoneType : "")
                + ")";
    }

}
