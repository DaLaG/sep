package cz.cvut.fel.lampedan.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * 
 * @author Daniel Lamper
 *
 */
@Entity(name = "CUSTOMER")
public class Customer implements Serializable
{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    //@PrimaryKey
    private Long id;
    private String firstName;
    private String surname;
    private String NIN;
    private String birthPlace;
    private String status;


    protected Customer()
    {
        //Singleton
    }


    public Customer(Customer c, String status)
    {
        super();
        this.firstName = c.getFirstName();
        this.surname = c.getSurname();
        this.NIN = c.getNIN();
        this.birthPlace = c.getBirthPlace();
        this.status = status;
    }


    public Customer(String firstName, String surname, String NIN, String birthPlace, String status)
    {
        super();
        this.firstName = firstName;
        this.surname = surname;
        this.NIN = NIN;
        this.birthPlace = birthPlace;
        this.status = status;

    }


    public Customer(Long id, String firstName, String surname, String NIN, String birthPlace, String status)
    {
        super();
        this.id = id;
        this.firstName = firstName;
        this.surname = surname;
        this.NIN = NIN;
        this.birthPlace = birthPlace;
        this.status = status;
    }


    public String getFirstName()
    {
        return firstName;
    }


    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }


    public String getSurname()
    {
        return surname;
    }


    public void setSurname(String surname)
    {
        this.surname = surname;
    }


    public String getNIN()
    {
        return NIN;
    }


    public void setNIN(String nIN)
    {
        NIN = nIN;
    }


    public String getBirthPlace()
    {
        return birthPlace;
    }


    public void setBirthPlace(String birthPlace)
    {
        this.birthPlace = birthPlace;
    }


    public String getStatus()
    {
        return status;
    }


    public void setStatus(String status)
    {
        this.status = status;
    }


    public Long getId()
    {
        return id;
    }


    /**
     * Sets the id value.
     * @param id the id to set
     */
    public void setId(Long id)
    {
        this.id = id;
    }


    @Override
    public String toString()
    {
        return "Customer (" + id + ", " + firstName + ", " + surname + ", " + status + ")";
    }
}
