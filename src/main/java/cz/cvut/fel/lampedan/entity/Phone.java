package cz.cvut.fel.lampedan.entity;

import java.io.Serializable;

import javax.jdo.annotations.ForeignKey;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Daniel Lamper
 *
 */
@Entity
@XmlRootElement
public class Phone implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Id
    @ForeignKey
    private Long userId;
    private int phoneNumberType;
    private String phoneNum;
    private String cityCode;
    private String countryCode;


    protected Phone()
    {
        //Singleton
    }


    public Phone(Long id, String phoneNumberType, String phoneNum)
    {
        super();
        this.userId = id;
        this.phoneNumberType = getTypeOfPhone(phoneNumberType);
        this.phoneNum = phoneNum;
    }


    public Phone(Long id, int phoneNumberType, String phoneNum, String countryCode)
    {
        super();
        this.userId = id;
        this.phoneNumberType = phoneNumberType;
        this.phoneNum = phoneNum;
        this.countryCode = countryCode;
    }


    public Phone(Long id, String phoneNumberType, String phoneNum, String countryCode)
    {
        super();
        this.userId = id;
        this.phoneNumberType = getTypeOfPhone(phoneNumberType);
        this.phoneNum = phoneNum;
        this.countryCode = countryCode;
    }


    public Phone(Long id, String phoneNumberType, String phoneNum, String cityCode, String countryCode)
    {
        super();
        this.userId = id;
        this.phoneNumberType = getTypeOfPhone(phoneNumberType);
        this.phoneNum = phoneNum;
        this.cityCode = cityCode;
        this.countryCode = countryCode;
    }


    protected int getTypeOfPhone(String phoneNumberType)
    {
        switch (phoneNumberType)
        {
            case "Mobilný telefón":
                return 0;
            case "Pevná linka":
                return 1;
        }
        return -1;
    }


    public Long getId()
    {
        return userId;
    }


    public void setId(Long id)
    {
        this.userId = id;
    }


    public int getPhoneNumberType()
    {
        return phoneNumberType;
    }


    public void setPhoneNumberType(int phoneNumberType)
    {
        this.phoneNumberType = phoneNumberType;
    }


    public String getPhoneNumberTypeString()
    {
        switch (phoneNumberType)
        {
            case 0:
                return "Mobilný telefón";
            case 1:
                return "Pevná linka";
        }
        return null;
    }


    public String getPhoneNum()
    {
        return phoneNum;
    }


    public void setPhoneNum(String phoneNum)
    {
        this.phoneNum = phoneNum;
    }


    public String getCityCode()
    {
        return cityCode;
    }


    public void setCityCode(String cityCode)
    {
        this.cityCode = cityCode;
    }


    public String getCountryCode()
    {
        return countryCode;
    }


    public void setCountryCode(String countryCode)
    {
        this.countryCode = countryCode;
    }


    @Override
    public String toString()
    {
        return "Phone (" + (userId != null ? userId + ", " : "") + phoneNumberType + ", "
                + (phoneNum != null ? phoneNum + ", " : "") + (cityCode != null ? cityCode + ", " : "")
                + (countryCode != null ? countryCode : "") + ")";
    }

}
