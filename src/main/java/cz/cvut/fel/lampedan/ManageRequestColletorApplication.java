package cz.cvut.fel.lampedan;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import cz.cvut.fel.lampedan.db.AddressRepository;
import cz.cvut.fel.lampedan.db.CustomerRepository;
import cz.cvut.fel.lampedan.db.PhoneRepository;
import cz.cvut.fel.lampedan.entity.Address;
import cz.cvut.fel.lampedan.entity.Customer;
import cz.cvut.fel.lampedan.entity.Phone;

/**
 * 
 * @author Daniel Lamper
 *
 */
@SpringBootApplication
public class ManageRequestColletorApplication //implements CommandLineRunner
{
    private static final Logger log = LoggerFactory.getLogger(ManageRequestColletorApplication.class);

    /*@Autowired
    JdbcTemplate jdbcTemplate;*/


    public static void main(String[] args)
    {

        SpringApplication.run(ManageRequestColletorApplication.class, args);
    }


    @Bean
    public CommandLineRunner demo(CustomerRepository customerRepository,
            AddressRepository addressRepository, PhoneRepository phoneRepository)
    {
        return (args) -> {
            // save a couple of customers
            customerRepository.save(new Customer("Kato", "Hahn", "1688092259099", "Bolivia", "Deactivated"));
            customerRepository.save(new Customer("Martha", "Case", "1616060383899", "Norway", "Refunded"));
            customerRepository.save(new Customer("Taylor", "Berger", "1688120825999", "Bhutan", "Suspended"));
            customerRepository.save(new Customer("Kaseem", "Cervantes", "1636081772699", "Moldova", "Refunded"));
            customerRepository.save(new Customer("Jenna", "Solomon", "1645030214199", "Belize", "Active"));
            customerRepository.save(new Customer("Heather", "Spence", "1693021802099", "Turks and Caicos Islands", "Active"));
            customerRepository.save(new Customer("Leah", "Talley", "1645112521799", "Guadeloupe", "Deactivated"));
            customerRepository.save(new Customer("Dolan", "Hill", "1622100321199", "Eritrea", "Active"));
            customerRepository.save(new Customer("Xenos", "Figueroa", "1636081553099", "Yemen", "Deactivated"));
            customerRepository.save(new Customer("Lucius", "Mccall", "1608032164499", "Aruba", "Active"));

            addressRepository.save(new Address(1L, "Baníkova Karlova Ves", 5289, "48494", "Billings", "Burkina Faso"));
            addressRepository.save(new Address(2L, "Antická Rusovce", 3143, "1207", "Mielec", "Bouvet Island"));
            addressRepository.save(new Address(3L, "Avarská Devín ", 1314, "53-032", "Kessenich", "British Indian Ocean Territory"));
            addressRepository.save(new Address(4L, "Bejdl Podunajské Biskupice", 3928, "3301XB", "Sparwood", "Christmas Island"));
            addressRepository.save(new Address(5L, "Blatnická Nové Mesto", 5657, "00830", "Sosnowiec", "Guinea"));
            addressRepository.save(new Address(6L, "Baníkova Karlova Ves", 4807, "78627", "Porto Cesareo", "Kuwait"));
            addressRepository.save(new Address(7L, "Belopotockého Staré Mesto", 4100, "3271", "Auckland", "Austria"));
            addressRepository.save(new Address(8L, "Antolská Petržalka", 715, "62-909", "Knokke-Heist", "Cayman Islands"));
            addressRepository.save(new Address(9L, "Barónka Rača", 5676, "6232", "Mainz", "Dominica"));
            addressRepository.save(new Address(10L, "Bedľová Karlova Ves", 1754, "4604", "Bergen op Zoom", "Angola"));

            phoneRepository.save(new Phone(1L, 0, "607740348", "+421"));
            phoneRepository.save(new Phone(2L, 0, "607268414", "+420"));
            phoneRepository.save(new Phone(3L, 1, "607449848", "+421"));
            phoneRepository.save(new Phone(4L, 1, "607666644", "+421"));
            phoneRepository.save(new Phone(5L, 1, "607941502", "+421"));
            phoneRepository.save(new Phone(6L, 0, "607836210", "+421"));
            phoneRepository.save(new Phone(7L, 0, "607325642", "+420"));
            phoneRepository.save(new Phone(8L, 1, "607025764", "+421"));
            phoneRepository.save(new Phone(9L, 1, "607102774", "+420"));
            phoneRepository.save(new Phone(10L, 1, "607777681", "+420"));

            log.info("Customers found with findAll():");
            log.info("-------------------------------");
            customerRepository.findAll().forEach(customer -> log.info(customer.toString()));
            log.info("");

            log.info("Address found with findAll():");
            log.info("-------------------------------");
            addressRepository.findAll().forEach(address -> log.info(address.toString()));
            log.info("");

            log.info("Phone found with findAll():");
            log.info("-------------------------------");
            phoneRepository.findAll().forEach(phone -> log.info(phone.toString()));
            log.info("");

            /*// fetch an individual customer by ID
            Customer customer = customerRepository.findOne(1L);
            log.info("Customer found with findOne(1L):");
            log.info("--------------------------------");
            log.info(customer.toString());
            log.info("");
            
            // fetch customers by last name
            log.info("Customer found with findByLastName('Bauer'):");
            log.info("--------------------------------------------");
            for (Customer bauer : customerRepository.findBySurname("Bauer"))
            {
                log.info(bauer.toString());
            }
            log.info("");*/
        };
    }
    /*@Override
    public void run(String... strings) throws Exception
    {
    
        log.info("Creating tables");
    
        jdbcTemplate.execute("DROP TABLE customers IF EXISTS");
        jdbcTemplate.execute("CREATE TABLE customers(" +
                "id SERIAL, firstname VARCHAR(255), surname VARCHAR(255), status VARCHAR(255))");
    
        // Split up the array of whole names into an array of first/last names
        List<Object[]> splitUpNames = Arrays.asList("John Woo Active", "Jeff Dean Active",
                "Josh Bloch Active", "Josh Long Active").stream()
                .map(name -> name.split(" "))
                .collect(Collectors.toList());
    
        // Use a Java 8 stream to print out each tuple of the list
        splitUpNames.forEach(name -> log.info(String.format("Inserting customer record for %s %s %s", name[0], name[1], name[2])));
    
        // Uses JdbcTemplate's batchUpdate operation to bulk load data
        jdbcTemplate.batchUpdate("INSERT INTO customers(firstname, surname, status) VALUES (?,?, ?)", splitUpNames);
    
        log.info("Querying for customer records where firstname = 'Josh':");
        jdbcTemplate.query(
                "SELECT id, firstname, surname, status FROM customers WHERE firstname = ?", new Object[] { "Josh" },
                (rs, rowNum) -> new Customer(rs.getLong("id"), rs.getString("firstname"), rs.getString("surname"), rs.getString("status")))
                .forEach(customer -> log.info(customer.toString()));
    }*/
}
