package cz.cvut.fel.lampedan;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cz.cvut.fel.lampedan.db.AddressRepository;
import cz.cvut.fel.lampedan.db.CustomerRepository;
import cz.cvut.fel.lampedan.db.PhoneRepository;
import cz.cvut.fel.lampedan.db.RequestRepository;
import cz.cvut.fel.lampedan.entity.Address;
import cz.cvut.fel.lampedan.entity.Customer;
import cz.cvut.fel.lampedan.entity.Phone;
import cz.cvut.fel.lampedan.entity.Request;

@Controller
public class ClientController
{

    private static final Logger log = LoggerFactory.getLogger(ManageRequestColletorApplication.class);

    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    AddressRepository addressRepository;
    @Autowired
    PhoneRepository phoneRepository;
    @Autowired
    RequestRepository requstRepository;

    Customer cus;
    Address adr;
    Phone phn;


    @RequestMapping("/")
    public String home()
    {
        return "redirect:/home";
    }


    //zobrazenie zoznamu klientov
    @RequestMapping("/home")
    public String returnHome()
    {
        return "/home";
    }


    @RequestMapping("/clients/listOfClients")
    public String create(Model model)
    {
        List<Customer> cusList = new ArrayList<>();
        cusList = customerRepository.findByStatusNot("Deactivated");
        log.info("Customer found with findall():");
        log.info("--------------------------------");
        cusList.forEach(customer -> log.info(customer.toString()));
        log.info("");
        model.addAttribute("cusList", cusList);
        return "clients/listOfClients";

    }


    //zobrazenie detailu klienta
    @RequestMapping(value = "/clients/clientDetail", method = RequestMethod.GET)
    public String getClientDetail(HttpServletRequest request, Model model)
    {

        Long user_id = Long.decode(request.getParameter("user_id"));
        log.info("user_id = " + user_id);
        cus = customerRepository.findOne(user_id);
        adr = addressRepository.findByUserId(user_id);
        phn = phoneRepository.findByUserId(user_id);
        model.addAttribute("customer", cus);
        model.addAttribute("address", adr);
        model.addAttribute("phone", phn);
        model.addAttribute("telNumType", phn.getPhoneNumberType() == 1 ? "Pevná linka" : "Mobilný telefón");
        return "/clients/clientDetail";
    }


    // TODO: treba pridať to že vlastne sa vytvorí zmenová požiadavka a v tabulke sa bude zobrazovať správna hodnota, tá nová
    //zobrazenie detailu klienta
    @RequestMapping(value = "/clients/clientDetail", method = RequestMethod.POST)
    public String editClient(HttpServletRequest request, Model model)
    {
        Customer c = new Customer(cus.getId(), request.getParameter("firstName"), request.getParameter("surname"),
                request.getParameter("NIN"), request.getParameter("birthPlace"), "Active");
        //customerRepository.save(c);
        Long user_id = c.getId();
        log.info("user_id = " + user_id);

        Address a = new Address(user_id, request.getParameter("streetName"),
                Integer.parseInt(request.getParameter("streetNum")), request.getParameter("postalCode"),
                request.getParameter("city"), request.getParameter("country"));
        //addressRepository.save(a);
        String phnNumType = request.getParameter("telNumType") == null ? phn.getPhoneNumberTypeString() : request.getParameter("telNumType");
        Phone p = new Phone(user_id, phnNumType,
                request.getParameter("phoneNum"), request.getParameter("countryCode"));
        //phoneRepository.save(p);

        requstRepository.save(new Request(user_id, request.getRemoteUser(), "zmena údajov", c, a, p));
        return "redirect:listOfClients";
    }


    //pridatnie noveho klienta
    @RequestMapping(value = "/clients/addClient", method = RequestMethod.GET)
    public String addPage(HttpServletRequest request)
    {
        return "/clients/addClient";
    }


    //pridatnie noveho klienta
    @RequestMapping(value = "/clients/addClient", method = RequestMethod.POST)
    public String add(HttpServletRequest request)
    {

        Customer c = new Customer(request.getParameter("firstName"), request.getParameter("surname"),
                request.getParameter("NIN"), request.getParameter("birthPlace"), "Active");
        customerRepository.save(c);
        Long user_id = c.getId();
        Address a = new Address(user_id, request.getParameter("streetName"),
                Integer.parseInt(request.getParameter("streetNum")), request.getParameter("postalCode"),
                request.getParameter("city"), request.getParameter("country"));
        addressRepository.save(a);

        Phone p = new Phone(user_id, request.getParameter("telNumType"),
                request.getParameter("phoneNum"), request.getParameter("countryCode"));
        phoneRepository.save(p);

        requstRepository.save(new Request(user_id, request.getRemoteUser(), "nový klient", c, a, p));
        return "redirect:listOfClients";
    }

    protected Long user_ID;


    //obnovenie klienta
    @RequestMapping("/clients/refund")
    public String refund(HttpServletRequest request)
    {
        if ("XMLHttpRequest".equals(request.getHeader("X-Requested-With")))
        {
            user_ID = Long.decode(request.getParameter("user_id"));
            return "/clients/dialogs/refund";

        }

        Customer c = new Customer(customerRepository.findOne(user_ID), "Refunded");
        Address a = addressRepository.findByUserId(user_ID);
        Phone p = phoneRepository.findByUserId(user_ID);

        requstRepository.save(new Request(user_ID, request.getRemoteUser(), "refundácia", c, a, p));

        log.info("Create request for refund user with ID = " + user_ID);

        /*log.info("Before update:");
        log.info("--------------------------------");
        log.info(customerRepository.findOne(user_ID).toString());
        log.info("");
        
        customerRepository.updateStatusById(user_ID, "Refunded");
        
        log.info("After update:");
        log.info("--------------------------------");
        log.info(customerRepository.findOne(user_ID).toString());
        log.info("");*/
        user_ID = -1L;
        return "redirect:listOfClients";
    }


    //suspendovanie klienta
    @RequestMapping("/clients/suspend")
    public String suspend(HttpServletRequest request, HttpServletResponse response, Model model)
    {
        if ("XMLHttpRequest".equals(request.getHeader("X-Requested-With")))
        {
            user_ID = Long.decode(request.getParameter("user_id"));
            return "/clients/dialogs/suspend";
        }
        Customer c = new Customer(customerRepository.findOne(user_ID), "Suspended");

        Address a = addressRepository.findByUserId(user_ID);
        Phone p = phoneRepository.findByUserId(user_ID);

        requstRepository.save(new Request(user_ID, request.getRemoteUser(), "suspendácia", c, a, p));

        log.info("Create request for suspend user with ID = " + user_ID);

        /*log.info("Create request for refund of user with ID" + user_ID);
        log.info("Before update:");
        log.info("--------------------------------");
        log.info(customerRepository.findOne(user_ID).toString());
        log.info("");
        
        customerRepository.updateStatusById(user_ID, "Suspended");
        
        log.info("After update:");
        log.info("--------------------------------");
        log.info(customerRepository.findOne(user_ID).toString());
        log.info("");*/
        user_ID = -1L;
        return "redirect:listOfClients";
    }


    //zmazanie klienta
    @RequestMapping("/clients/delete")
    public String delete(HttpServletRequest request, HttpServletResponse response, Model model)
    {
        if ("XMLHttpRequest".equals(request.getHeader("X-Requested-With")))
        {
            user_ID = Long.decode(request.getParameter("user_id"));
            return "/clients/dialogs/delete";
        }
        Customer c = new Customer(customerRepository.findOne(user_ID), "Deactivated");
        Address a = addressRepository.findByUserId(user_ID);
        Phone p = phoneRepository.findByUserId(user_ID);

        requstRepository.save(new Request(user_ID, request.getRemoteUser(), "deaktivácia", c, a, p));

        log.info("Create request for delete user with ID = " + user_ID);

        /*log.info("Before update:");
        log.info("--------------------------------");
        log.info(customerRepository.findOne(user_ID).toString());
        log.info("");
        
        customerRepository.updateStatusById(user_ID, "Deactivated");
        
        log.info("After update:");
        log.info("--------------------------------");
        log.info(customerRepository.findOne(user_ID).toString());
        log.info("");*/
        user_ID = -1L;
        return "redirect:listOfClients";
    }

}
