/**
 * Copyright 2016 Bosch Automotive Service Solutions Ltd.
 * All Rights reserved.
 */
package cz.cvut.fel.lampedan;

import javax.servlet.http.HttpServletRequest;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.soap.SOAPFaultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cz.cvut.fel.lampedan.db.RequestRepository;
import cz.cvut.fel.lampedan.entity.Request;

/**
 * @author Daniel Lamper
 *
 */

@Controller
public class APIController
{

    private static final Logger log = LoggerFactory.getLogger(ManageRequestColletorApplication.class);

    @Autowired
    RequestRepository requstRepository;


    @RequestMapping("/clients/load")
    public String loadCustomers()
    {
        log.info("Load all clients from API");
        try
        {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            String url = "http://localhost:8088/mockCustomerDatabaseSOAP";
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(0, null), url);

            // Process the SOAP Response
            printSOAPResponse(soapResponse);

            soapConnection.close();
        }
        catch (Exception e)
        {
            System.err.println("Error occurred while sending SOAP Request to Server");
            e.printStackTrace();
        }
        return "redirect:/";
    }


    @RequestMapping(value = "/clients/loadCustomerDetail", method = RequestMethod.GET)
    public String loadCustomerDetail(HttpServletRequest request)
    {

        String user_id = request.getParameter("user_id");
        log.info("Load customer detail from API, customer id = " + user_id);
        try
        {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            String url = "http://localhost:8088/mockCustomerDatabaseSOAP";
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(1, user_id), url);

            // Process the SOAP Response
            printSOAPResponse(soapResponse);

            soapConnection.close();
        }
        catch (Exception e)
        {
            System.err.println("Error occurred while sending SOAP Request to Server");
            e.printStackTrace();
        }
        return "redirect:/";
    }


    @RequestMapping(value = "/clients/saveCustomer", method = RequestMethod.GET)
    public String createCustomerChange(HttpServletRequest request)
    {
        String user_id = request.getParameter("user_id");
        log.info("Save customer via API, customer id = " + user_id);
        try
        {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            String url = "http://localhost:8088/mockCustomerDatabaseSOAP";
            Request r = requstRepository.findByUserId(Long.decode(user_id));
            //SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(2, user_id), url);
            SOAPMessage soapResponse = soapConnection.call(
                    r.getCustomerDetailTypeDocument(), url);
            // Process the SOAP Response
            printSOAPResponse(soapResponse);

            soapConnection.close();
        }
        catch (SOAPFaultException | UnsupportedOperationException | SOAPException e)
        {
            System.err.println("Error occurred while sending SOAP Request to Server");
            e.printStackTrace();
        }
        return "redirect:/";
    }


    private SOAPMessage createSOAPRequest(int typeOfRequest, String id) throws Exception
    {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String serverURI = "http://www.cvut.cz/FEL/";

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration("fel", serverURI);

        // SOAP Body
        SOAPBody soapBody = null;

        switch (typeOfRequest)
        {
            case 0:
                soapBody = readAllCustomersRequest(envelope);
                break;
            case 1:
                soapBody = readCustomerDetailRequest(envelope, id);
                break;

        }
        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", serverURI);

        soapMessage.saveChanges();

        /* Print the request message */
        log.info("Request SOAP Message = ");
        soapMessage.writeTo(System.out);
        System.out.println();

        return soapMessage;
    }


    private SOAPBody readAllCustomersRequest(SOAPEnvelope envelope) throws Exception
    {

        SOAPBody soapBody = envelope.getBody();

        SOAPElement soapBodyElem = soapBody.addChildElement("ReadAllCustomers", "fel");

        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("from");
        soapBodyElem1.addTextNode("0");

        SOAPElement soapBodyElem2 = soapBodyElem.addChildElement("count");
        soapBodyElem2.addTextNode("123");

        return soapBody;
    }


    private SOAPBody readCustomerDetailRequest(SOAPEnvelope envelope, String id) throws Exception
    {

        SOAPBody soapBody = envelope.getBody();

        SOAPElement soapBodyElem = soapBody.addChildElement("ReadCustomerDetails", "fel");

        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("id");
        soapBodyElem1.addTextNode(id);

        return soapBody;
    }


    /*   private SOAPBody createCustomerChangeRequest(SOAPEnvelope envelope, String id) throws Exception
    {
        Request r = requstRepository.findByUserId(Long.decode(id));
        r.getXml_customerDetailType();
        SOAPBody soapBody = envelope.getBody();
        soapBody.addDocument(r.getCustomerDetailTypeDocument());
        //soapBody.addBodyElement();
    
        return soapBody;
    }*/

    /**
     * Method used to print the SOAP Response
     */
    private static void printSOAPResponse(SOAPMessage soapResponse)
    {
        try
        {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            Source sourceContent = soapResponse.getSOAPPart().getContent();
            System.out.print("\nResponse SOAP Message = ");
            StreamResult result = new StreamResult(System.out);
            transformer.transform(sourceContent, result);
        }
        catch (SOAPException | TransformerException e)
        {
            System.err.println("Error occurred while reading SOAP Reaponse");
            e.printStackTrace();
        }
    }
}
