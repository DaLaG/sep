package cz.cvut.fel.lampedan.db;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cz.cvut.fel.lampedan.entity.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long>
{

    List<Customer> findByFirstName(String firstName);


    List<Customer> findBySurname(String surname);


    List<Customer> findByStatusNot(String status);


    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("UPDATE CUSTOMER c SET c.status = :status WHERE c.id = :user_id")
    int updateStatusById(@Param("user_id") Long user_id, @Param("status") String status);
}
