/**
 * Copyright 2016 Bosch Automotive Service Solutions Ltd.
 * All Rights reserved.
 */
package cz.cvut.fel.lampedan.db;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.cvut.fel.lampedan.entity.Request;

/**
 * @author Daniel Lamper
 *
 */
public interface RequestRepository extends JpaRepository<Request, Long>
{

    Request findByUserId(Long userId);
}
