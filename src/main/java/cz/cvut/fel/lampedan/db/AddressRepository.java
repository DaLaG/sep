package cz.cvut.fel.lampedan.db;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.cvut.fel.lampedan.entity.Address;

public interface AddressRepository extends JpaRepository<Address, Long>
{

    List<Address> findByCity(String city);


    List<Address> findByCountry(String country);


    Address findByUserId(Long userId);

}
