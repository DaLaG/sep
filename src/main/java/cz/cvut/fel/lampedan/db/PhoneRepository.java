package cz.cvut.fel.lampedan.db;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cz.cvut.fel.lampedan.entity.Phone;

public interface PhoneRepository extends JpaRepository<Phone, Long>
{

    List<Phone> findByPhoneNumberType(int phoneNumType);


    List<Phone> findByPhoneNum(String phoneNum);


    List<Phone> findByCountryCode(String countryCode);


    Phone findByUserId(Long userId);
}
