package cz.cvut.fel.lampedan;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import cz.cvut.fel.lampedan.db.CustomerRepository;
import cz.cvut.fel.lampedan.db.RequestRepository;
import cz.cvut.fel.lampedan.entity.Address;
import cz.cvut.fel.lampedan.entity.Customer;
import cz.cvut.fel.lampedan.entity.Phone;
import cz.cvut.fel.lampedan.entity.Request;

@Controller
public class RequestController
{

    private static final Logger log = LoggerFactory.getLogger(ManageRequestColletorApplication.class);

    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    RequestRepository requstRepository;


    //zobrazenie listu poziadavok
    @RequestMapping("/requests/showRequests")
    public String show(HttpServletRequest request, Model model)
    {

        List<Request> requstList = new ArrayList<>();
        requstList = requstRepository.findAll();
        log.info("Requests found with findall():");
        log.info("--------------------------------");
        requstList.forEach(req -> log.info(req.toString()));
        log.info("");
        model.addAttribute("reqList", requstList);
        return "requests/showRequests";
    }

    Long request_ID;


    //zmazanie klienta
    @RequestMapping("/requests/requestDetail")
    public String detail(HttpServletRequest request, Model model) throws JAXBException, Exception
    {
        if ("XMLHttpRequest".equals(request.getHeader("X-Requested-With")))
        {
            request_ID = Long.decode(request.getParameter("request_id"));
            Request r = requstRepository.findOne(request_ID);

            if (request.getParameter("requestType").equals("zmena údajov"))
            {

                Phone p = getPhoneFromXML(r.getXml_phoneType(), r.getUserId());
                log.info(p.toString());
                Address a = getAddressFromXML(r.getXml_addressType(), r.getUserId());
                log.info(a.toString());
                Customer c = getCustomerFromXML(r.getXml_customerDetailType(), r.getUserId());
                log.info(c.toString());
                model.addAttribute("phone", p);
                model.addAttribute("address", a);
                model.addAttribute("customer", c);
                model.addAttribute("telNumType", p.getPhoneNumberType() == 1 ? "Pevná linka" : "Mobilný telefón");
                return "/requests/requestDetail";
            }
            else
            {
                Customer c = customerRepository.findOne(r.getUserId());
                model.addAttribute("customer", c);
                model.addAttribute("requestType", r.getRequestType());
                return "/requests/requestShortDetail";

            }
        }

        request_ID = -1L;
        return "redirect:showRequests";
    }


    //zmazanie klienta
    @RequestMapping("/requests/delete")
    public String delete(HttpServletRequest request, Model model)
    {
        if ("XMLHttpRequest".equals(request.getHeader("X-Requested-With")))
        {
            request_ID = Long.decode(request.getParameter("request_id"));
            return "/requests/delete";
        }

        requstRepository.delete(request_ID);

        request_ID = -1L;
        return "redirect:showRequests";
    }


    private Phone getPhoneFromXML(String xml_phoneType, Long user_id)
    {
        Phone p = new Phone(user_id, 0, "", "");
        try
        {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;

            builder = factory.newDocumentBuilder();

            Document document = builder.parse(new InputSource(new StringReader(xml_phoneType)));

            NodeList nl = document.getFirstChild().getChildNodes();
            p.setPhoneNumberType(Integer.parseInt(nl.item(0).getTextContent()));
            p.setPhoneNum(nl.item(1).getTextContent());
            if (nl.getLength() > 2)
            {
                p.setCountryCode(nl.item(2).getTextContent());
            }

            return p;
        }
        catch (SAXException | IOException | ParserConfigurationException e)
        {
            log.error(e.getMessage());
        }

        return p;
    }


    private Address getAddressFromXML(String xml_addressType, Long userId)
    {
        Address a = new Address(userId, "", 0, "", "", "");
        try
        {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;

            builder = factory.newDocumentBuilder();

            Document document = builder.parse(new InputSource(new StringReader(xml_addressType)));

            NodeList nl = document.getFirstChild().getChildNodes();
            a.setStreetName(nl.item(0).getTextContent());
            a.setStreetNum(Integer.parseInt(nl.item(1).getTextContent()));
            a.setPostalCode(nl.item(2).getTextContent());
            a.setCity(nl.item(3).getTextContent());
            a.setCountry(nl.item(4).getTextContent());

            return a;
        }
        catch (SAXException | IOException | ParserConfigurationException e)
        {
            log.error(e.getMessage());
        }

        return a;
    }


    private Customer getCustomerFromXML(String xml_customerType, Long userId)
    {

        Customer c = new Customer(userId, "", "", "", "", "");
        try
        {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;

            builder = factory.newDocumentBuilder();

            Document document = builder.parse(new InputSource(new StringReader(xml_customerType)));

            NodeList nl = document.getFirstChild().getChildNodes().item(2).getChildNodes();
            c.setFirstName(nl.item(0).getTextContent());
            c.setSurname(nl.item(1).getTextContent());
            c.setNIN(nl.item(4).getTextContent());
            c.setBirthPlace(nl.item(5).getTextContent());

            return c;
        }
        catch (SAXException | IOException | ParserConfigurationException e)
        {
            log.error(e.getMessage());
        }

        return c;
    }
}
